# README #

We tried making an app which will detect the user's location and provide relevant information in the UTD campus area. For example, if the user is near the ECSS building, floor wise maps and information will be displayed on the screen. 

Having had experience primarily as web developers, we tried to use the MIT app inventor to make the android app. It's a work in progress.

### Contributors ###

* Dhruv Sangvikar
* Lakshmi Sharma
* Aditi Ghamande